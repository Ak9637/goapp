package com.sample.go.tdd

import android.view.View
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import org.junit.runner.RunWith
import com.sample.go.tdd.screens.main.MainActivity
import androidx.test.rule.ActivityTestRule
import com.sample.go.tdd.root.Constants
import com.sample.go.tdd.screens.main.MainContracts
import com.sample.go.tdd.screens.main.MainViewModel
import com.sample.go.tdd.testing_helpers.MockData
import org.hamcrest.CoreMatchers.containsString
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.AdditionalMatchers.not
import org.mockito.Mock


@RunWith(AndroidJUnit4::class)
class MainActivityTest {

    @Rule
    var mainActivityTestRule = ActivityTestRule(MainActivity::class.java)


    @Before
    fun setUp(){

    }

    @Test
    fun a_checkIfLoadedCorrectly(){
        onView((withId(R.id.loadingLayout))).check(matches(not(isDisplayed())))
        onView((withId(R.id.errorLayout))).check(matches(not(isDisplayed())))
        onView((withId(R.id.dataLayout))).check(matches(not(isDisplayed())))
    }
    @Test
    fun b_checkIfLoadingIsCorrectlyShown(){
        mainActivityTestRule.activity.showLoading()
        mainActivityTestRule.activity.removeError()
        mainActivityTestRule.activity.hideData()
        
        onView((withId(R.id.loadingLayout))).check(matches(isDisplayed()))
        onView((withId(R.id.errorLayout))).check(matches(not(isDisplayed())))
        onView((withId(R.id.dataLayout))).check(matches(not(isDisplayed())))
    }

    @Test
    fun c_checkIfErrorIsCorrectlyShown(){
        mainActivityTestRule.activity.stopLoading()
        mainActivityTestRule.activity.showError(Constants.DEFAULT_ERROR_MESSAGE)
        mainActivityTestRule.activity.hideData()

        onView((withId(R.id.loadingLayout))).check(matches(not(isDisplayed())))
        onView((withId(R.id.errorLayout))).check(matches(isDisplayed()))
        onView((withId(R.id.dataLayout))).check(matches(not(isDisplayed())))

        onView((withId(R.id.errorMessageTv))).check(matches(withText(containsString(Constants.DEFAULT_ERROR_MESSAGE))));
    }

    @Test
    fun d_checkIfDataIsCorrectShown(){
        mainActivityTestRule.run {
            activity.stopLoading()
            activity.removeError()
            activity.showData(MockData.mockForecastData)
        }

        onView((withId(R.id.loadingLayout))).check(matches(not(isDisplayed())))
        onView((withId(R.id.errorLayout))).check(matches(not(isDisplayed())))
        onView((withId(R.id.dataLayout))).check(matches(isDisplayed()))

        onView((withId(R.id.mainDataTempDisplayTv)))
            .check(matches(withText(containsString("${MockData.mockForecastData.current!!.tempC}"))))

        onView((withId(R.id.mainDataDescTv)))
            .check(matches(withText(containsString("${MockData.mockForecastData.location!!.name!!}"))))


    }

    @Test
    fun d_checkIfRetryIsCorrectlyShown(){
        mainActivityTestRule.activity.stopLoading()
        mainActivityTestRule.activity.showError(Constants.DEFAULT_ERROR_MESSAGE)
        mainActivityTestRule.activity.hideData()

        onView((withId(R.id.loadingLayout))).check(matches(not(isDisplayed())))
        onView((withId(R.id.errorLayout))).check(matches(isDisplayed()))
        onView((withId(R.id.dataLayout))).check(matches(not(isDisplayed())))

        onView((withId(R.id.errorMessageTv))).check(matches(withText(containsString(Constants.DEFAULT_ERROR_MESSAGE))));

        onView((withId(R.id.errorRetryBt))).perform(click())

        mainActivityTestRule.activity.showLoading()
        mainActivityTestRule.activity.removeError()
        mainActivityTestRule.activity.hideData()

        onView((withId(R.id.loadingLayout))).check(matches(isDisplayed()))
        onView((withId(R.id.errorLayout))).check(matches(not(isDisplayed())))
        onView((withId(R.id.dataLayout))).check(matches(not(isDisplayed())))

    }



}