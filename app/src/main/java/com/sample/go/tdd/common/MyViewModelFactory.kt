package com.sample.go.tdd.common

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

/**
 * Wrapper for creating viewmodel instances with custom dependencies , refer DI logic for this
 */

class MyViewModelFactory<T: ViewModel>(model: T) : ViewModelProvider.Factory {
    private val viewModel: T =model

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return viewModel as T
    }
}