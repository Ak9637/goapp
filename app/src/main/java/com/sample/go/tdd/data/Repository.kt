package com.sample.go.tdd.data

import androidx.lifecycle.LiveData
import com.sample.go.tdd.data.models.ForecastData
import com.sample.go.tdd.data.models.ForecastDataContainer


/*
* Access points for repository
*/
interface Repository {
    fun getForecastForLocation(lat:Double,long:Double,days:Int)
    fun getForecastListener():LiveData<ForecastDataContainer>

}