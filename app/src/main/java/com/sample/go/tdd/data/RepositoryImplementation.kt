package com.sample.go.tdd.data

import android.annotation.SuppressLint
import android.os.Handler
import androidx.annotation.VisibleForTesting
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.sample.go.tdd.data.models.*
import com.sample.go.tdd.data.remote.ApiService
import com.sample.go.tdd.root.Constants
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import retrofit2.Response


/*
* This class implements the methods and calls defined by repository and would never ne exposed directly
* Currently All local and remote data access is done only this class
*/

open class RepositoryImplementation(
    private val apiService: ApiService,
    private val key:String) : Repository {



    private var forecastData: MutableLiveData<ForecastDataContainer> = MutableLiveData()



    private val forecastObserver=object: io.reactivex.Observer<Response<ForecastData?>>{
        override fun onComplete() {
        }

        override fun onSubscribe(d: Disposable) {
        }

        override fun onNext(t: Response<ForecastData?>) {
            if(t.code()==200 ){
                if(t.body()==null){
                    forecastData.value =
                        ForecastDataContainer(null, Constants.DEFAULT_ERROR_MESSAGE)
                }else{
                    forecastData.value =
                        ForecastDataContainer(t.body(), null)
                }

            }else{
                forecastData.value =
                    ForecastDataContainer(null, Constants.DEFAULT_ERROR_MESSAGE)
            }



        }

        override fun onError(e: Throwable) {
            forecastData.value =
                ForecastDataContainer(null,e.message)


        }
    }






    override fun getForecastListener(): LiveData<ForecastDataContainer> {
        return forecastData

    }

    @VisibleForTesting
    fun getForcastObserver():io.reactivex.Observer<Response<ForecastData?>>{
        return forecastObserver
    }









    @SuppressLint("CheckResult")
    override fun getForecastForLocation(lat: Double, long: Double,days:Int) {

        apiService.requestForecastForLocation(key,"$lat,$long",days)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeWith(forecastObserver)

    }


}