package com.sample.go.tdd.data.models

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class Forecast(

	@field:SerializedName("forecastday")
	val forecastday: List<ForecastdayItem>? = null
)