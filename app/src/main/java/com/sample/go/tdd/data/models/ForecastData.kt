package com.sample.go.tdd.data.models

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class ForecastData(

	@field:SerializedName("current")
	val current: Current? = null,

	@field:SerializedName("alert")
	val alert: Alert? = null,

	@field:SerializedName("location")
	val location: Location? = null,

	@field:SerializedName("forecast")
	val forecast: Forecast? = null
)