package com.sample.go.tdd.data.models

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class ForecastdayItem(

	@field:SerializedName("date")
	val date: String? = null,

	@field:SerializedName("astro")
	val astro: Astro? = null,

	@field:SerializedName("date_epoch")
	val dateEpoch: Int? = null,

	@field:SerializedName("day")
	val day: Day? = null
)