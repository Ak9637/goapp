package com.sample.go.tdd.data.remote

object ApiData {
    const val FORECAST="forecast.json"
    const val LOCATION_REQUEST_PARAM="q"
    const val KEY="key"
    const val DAYS="days"

}