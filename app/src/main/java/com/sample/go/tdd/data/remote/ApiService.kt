package com.sample.go.tdd.data.remote

import com.sample.go.tdd.data.models.ForecastData
import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Path
import retrofit2.http.Query

/*
* All Api call routes
*/
interface ApiService{


    @GET(ApiData.FORECAST)
    fun requestForecastForLocation(@Query(ApiData.KEY) key:String, @Query(ApiData.LOCATION_REQUEST_PARAM) locationString:String, @Query(ApiData.DAYS) days:Int): Observable<Response<ForecastData?>>

}