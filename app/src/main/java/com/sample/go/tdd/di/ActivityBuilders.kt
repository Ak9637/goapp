package com.sample.go.tdd.di

import android.app.Activity
import com.sample.go.tdd.screens.main.MainActivity
import com.sample.go.tdd.screens.main.di.MainSubComponent
import dagger.Binds
import dagger.Module
import dagger.android.ActivityKey
import dagger.android.AndroidInjector
import dagger.multibindings.IntoMap


/**
 * App Level Di
 * All Activity with di need to added to activity buider for DI tree
 * Simply copy paste and change the names
 */


@Module
abstract class ActivityBuilders{

    @Binds
    @IntoMap
    @ActivityKey(MainActivity::class)
    abstract fun bindMainActivityInjectorFactory(builder: MainSubComponent.Builder): AndroidInjector.Factory<out Activity>



}