package com.sample.go.tdd.di

import android.app.Application
import android.content.Context
import com.sample.go.tdd.root.ThisApplication
import com.sample.go.tdd.screens.main.di.MainSubComponent
import dagger.Module
import dagger.Provides


/**
 * App Module
 * All Sub-dependencies , i.e all independent dependencies like activities , singletons ,etc modules are added here
 * for access to APP level dependencies
 */

@Module(
    subcomponents = [
    MainSubComponent::class
    ]
)


class AppModule {


    @Provides
    @MyAppScope
    internal fun provideApplication(application: ThisApplication): Application {
        return application
    }
}