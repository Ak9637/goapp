package com.sample.go.tdd.di

import com.sample.go.tdd.root.ThisApplication
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.support.AndroidSupportInjectionModule

/**
 * App Component
 * Contains all App level Dependencies modules
 */


@MyAppScope
@Component(modules = arrayOf(
    AndroidSupportInjectionModule::class,
    AndroidInjectionModule::class,
    AppModule::class,
    DataModule::class,
    ActivityBuilders::class,
    CredentialsModule::class,
    RepositoryModule::class))
interface ApplicationComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun injectApplication(application: ThisApplication): Builder
        fun build(): ApplicationComponent
    }

    abstract fun inject(application: ThisApplication)


}
