package com.sample.go.tdd.di


import com.sample.go.tdd.BuildConfig
import com.sample.go.tdd.root.Constants
import dagger.Module
import dagger.Provides
import javax.inject.Named

/**
 *Module providing the Credential for transcations
 */
@Module
class CredentialsModule{

    @Provides
    @Named(Constants.API_BASE)
    @MyAppScope
    fun getBaseUrl(): String {
        return BuildConfig.SERVER_URL
    }

    @Provides
    @Named(Constants.API_KEY)
    @MyAppScope
    fun getApiKey(): String {
        return "eb280625d0b04f6282362804192808"
    }

}