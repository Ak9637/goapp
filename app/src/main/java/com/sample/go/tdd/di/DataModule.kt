package com.sample.go.tdd.di


import com.sample.go.tdd.helpers.SharedPreferencesManager
import com.sample.go.tdd.root.ThisApplication
import dagger.Module
import dagger.Provides
import javax.inject.Named


/**
 *Module providing the Data Layer dependencies
 */
@Module
class DataModule {

    companion object {
        const val APP_PREFERENCES="App_Pref"
        val SHARED_PREF = "my_pref"

    }


    @Provides
    @MyAppScope
    @Named(APP_PREFERENCES)

    fun getSharedPreferenceHelper(): SharedPreferencesManager {
        return SharedPreferencesManager(ThisApplication.mInstance, SHARED_PREF,true)
    }



}