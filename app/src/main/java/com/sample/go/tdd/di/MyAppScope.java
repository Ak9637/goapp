package com.sample.go.tdd.di;

import javax.inject.Scope;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 *Interface restricting scope of dependencies to app lifecycle
 */
@Scope
@Retention(RetentionPolicy.CLASS)
public @interface MyAppScope {
}
