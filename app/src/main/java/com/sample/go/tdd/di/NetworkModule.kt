package com.sample.go.tdd.di

import android.util.Log
import com.google.gson.Gson
import com.sample.go.tdd.BuildConfig
import com.sample.go.tdd.data.remote.ApiService
import com.sample.go.tdd.root.ThisApplication
import dagger.Module
import dagger.Provides
import okhttp3.Cache
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import java.io.File
import java.util.concurrent.TimeUnit
import javax.inject.Named

/**
 *Module providing network dependencies
 */
@Module
open class NetworkModule {


    @Provides
    @MyAppScope
    fun httpLoggingInterceptor(): Interceptor {

        return HttpLoggingInterceptor(HttpLoggingInterceptor.Logger { message ->
                Log.i(ThisApplication.APP_TAG, message)
            })
                .setLevel(HttpLoggingInterceptor.Level.BODY)


    }

    @Provides
    @MyAppScope
    fun cache(cacheFile: File): Cache {
        return Cache(cacheFile, (20 * 1024 * 1024).toLong())  // 20 MB Cache File
    }

    @Provides
    @MyAppScope
    @Named("baseUrl")
    fun getBaseUrl(): String {
        return BuildConfig.SERVER_URL.trim()
    }


    @Provides
    @MyAppScope
    fun cacheFile(): File {
        return File(ThisApplication.mInstance.cacheDir, "okhttp-cache")
    }

    @Provides
    @MyAppScope
    fun okHttpClient(interceptor: Interceptor, cache: Cache): OkHttpClient {
        return OkHttpClient.Builder()
            .addInterceptor(interceptor)
            .connectTimeout((BuildConfig.NETWORK_TIMEOUT).toLong(), TimeUnit.SECONDS)
            .readTimeout(BuildConfig.NETWORK_TIMEOUT.toLong(), TimeUnit.SECONDS)
            .cache(cache)
            .build()
    }

    @Provides
    @MyAppScope
    open fun getApiService(retrofit: Retrofit): ApiService {
        return retrofit.create(ApiService::class.java)
    }

    @Provides
    @MyAppScope
    fun getGson(): Gson {
        return Gson()
    }

}
