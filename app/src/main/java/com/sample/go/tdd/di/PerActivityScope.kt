package com.sample.go.tdd.di


import javax.inject.Scope

/**
 *Interface restricting scope of dependencies to per activity lifecycle
 */
@Scope
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
annotation class PerActivityScope
