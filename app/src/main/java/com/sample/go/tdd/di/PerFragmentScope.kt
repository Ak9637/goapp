package com.sample.go.tdd.di

import javax.inject.Scope


/**
 *Interface restricting scope of dependencies to per fragment lifecycle
 */
@Scope
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
annotation class PerFragmentScope