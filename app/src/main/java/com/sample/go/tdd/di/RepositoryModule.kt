package com.sample.go.tdd.di

import com.sample.go.tdd.data.Repository
import com.sample.go.tdd.data.RepositoryImplementation
import com.sample.go.tdd.data.remote.ApiService
import com.sample.go.tdd.helpers.SharedPreferencesManager
import com.sample.go.tdd.root.Constants
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Named


/**
 *Module providing data repositories dependencies
 */
@Module(includes = arrayOf(RetrofitModule::class))
open class RepositoryModule{



    @Provides
    @MyAppScope
    open fun getApiRepository(
        apiService: ApiService,
        @Named(Constants.API_KEY) apiKey:String
    ): Repository {
        return RepositoryImplementation(apiService,apiKey)
    }


}