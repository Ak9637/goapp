package com.sample.go.tdd.di

import android.os.Build
import com.google.gson.Gson
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import com.sample.go.tdd.BuildConfig
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Named
import android.os.AsyncTask



/**
 *Module providing retrofit dependencies
 */

@Module(includes = arrayOf(NetworkModule::class))
class RetrofitModule {
    @Provides
    @MyAppScope
    fun retrofit(gson: Gson, client: OkHttpClient,@Named("baseUrl") url:String): Retrofit {

        return Retrofit.Builder()
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create(gson))
            .client(client)
            .baseUrl(url)
            .build()


    }



}
