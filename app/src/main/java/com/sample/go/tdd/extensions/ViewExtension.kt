package com.sample.go.tdd.extensions

import android.view.View
import kotlinx.android.synthetic.main.activity_main.*

fun View.turnGone(){
    if(this.visibility!= View.GONE){
        this.visibility=View.GONE
    }
}
fun View.turnVisible(){
    if(this.visibility!= View.VISIBLE){
        this.visibility=View.VISIBLE
    }
}