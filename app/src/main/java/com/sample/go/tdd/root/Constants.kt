package com.sample.go.tdd.root

object Constants{
    const val API_BASE="API_BASE"
    const val API_KEY="API_KEY"
    const val DEFAULT_ERROR_MESSAGE="Something went wrong, please try again"

}