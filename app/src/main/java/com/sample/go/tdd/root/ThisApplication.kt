package com.sample.go.tdd.root

import android.annotation.SuppressLint
import android.app.Activity
import android.app.ActivityManager
import android.app.ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND
import android.app.ActivityManager.RunningAppProcessInfo.IMPORTANCE_VISIBLE
import android.app.Application
import android.content.*
import android.os.Build
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatDelegate
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import androidx.multidex.BuildConfig
import androidx.multidex.MultiDex
import com.sample.go.tdd.di.DaggerApplicationComponent
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import dagger.android.support.HasSupportFragmentInjector
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit


import javax.inject.Inject
import java.lang.Exception
import java.lang.ref.WeakReference

/**
 * Application class
 * This class is responsible for listening to internet connection and user session status and
 * Initialse all App level requirements
 */

open class ThisApplication: Application(), HasActivityInjector {



    companion object {
        const val APP_TAG = "TDD_APP"
        @Volatile
        lateinit var mInstance: ThisApplication

    }

    private val unCaughtExceptionHandler = Thread.UncaughtExceptionHandler { _, e ->
        run {
            // catches and prints any unhandled exception in entire app
            e.printStackTrace()
        }
    }


    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Activity>



    override fun onCreate() {

        mInstance = this


        DaggerApplicationComponent.builder().injectApplication(this).build().inject(this)
        //MultiDex.install(this)
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)
        inits()
        initLibraries()

        super.onCreate()


    }

    private fun inits() {




    }

    private fun initLibraries() {
        Thread.setDefaultUncaughtExceptionHandler(unCaughtExceptionHandler)
    }

    @SuppressLint("CheckResult")
    override fun onTrimMemory(level: Int) {
        super.onTrimMemory(level)
        Log.d(APP_TAG, level.toString() + "")
        System.gc()

        /**
         * If system reaches criticical memory states , clear space
         */

        io.reactivex.Observable.just(level)
            .observeOn(Schedulers.io())
            .subscribeOn(Schedulers.io())
            .doOnError { e -> e.printStackTrace() }
            .subscribe { level ->
                run {
                    if (level <= ComponentCallbacks2.TRIM_MEMORY_RUNNING_LOW) {

                    }

                }
            }

    }


    override fun activityInjector(): AndroidInjector<Activity>? {
        return dispatchingAndroidInjector
    }



}