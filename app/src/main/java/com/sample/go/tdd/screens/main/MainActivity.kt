package com.sample.go.tdd.screens.main

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.animation.AnimationUtils
import androidx.annotation.VisibleForTesting
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.sample.go.tdd.R
import com.sample.go.tdd.data.models.ForecastData
import com.sample.go.tdd.extensions.turnGone
import com.sample.go.tdd.extensions.turnVisible
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.error.*
import kotlinx.android.synthetic.main.loading.*
import kotlinx.android.synthetic.main.main_data.*
import javax.inject.Inject
import javax.inject.Named
import androidx.recyclerview.widget.DividerItemDecoration



class MainActivity : AppCompatActivity(),MainContracts.View {



    @Inject
    @field: Named(MainContracts.TAG)
    lateinit var viewModel: MainContracts.ViewModel

    private lateinit var mAdapter :MainAdapter


    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initialse()
        getData()

    }





    override fun initialse() {
        // This will initialse the functionary of viewmodel


        viewModel.runSetup()
        errorRetryBt?.setOnClickListener {
            getData()
        }

        // Start listening to viewmodel
        viewModel?.listenForLoadingStateChanges()!!.observe(this, Observer {
            if(it){
                showLoading()
            }else{
                stopLoading()
            }
        })
        viewModel?.listenForErrors()!!.observe(this, Observer {
            if(it==null || it.isEmpty()){
                removeError()
            }else{
                showError(it)
            }
        })
        viewModel?.listenForForecasts()!!.observe(this, Observer {
            if(it!=null){
                showData(it)
            }else{
                showError(resources.getString(R.string.SomethingWentWrongMessage))
            }
        })

        mainDataRv?.layoutManager =LinearLayoutManager(this)

        mainDataRv?.addItemDecoration(DividerItemDecoration(this,
            DividerItemDecoration.VERTICAL))



    }

    override fun showLoading() {
        runOnUiThread {
            loadingLayout.turnVisible()
            loadingIv.startAnimation(AnimationUtils.loadAnimation(this,R.anim.rotate))
        }
    }

    override fun stopLoading() {
        runOnUiThread {
            loadingLayout.turnGone()
        }
    }

    @SuppressLint("SetTextI18n")
    override fun showData(forecastData: ForecastData) {
        runOnUiThread {

            mainDataTempDisplayTv?.text="${forecastData.current!!.tempC!!}\u2103"

            if(forecastData.location!!.name!=null && !forecastData.location!!.name!!.isEmpty()){
                mainDataDescTv?.text = forecastData.location!!.name!!


            }else if(forecastData.location.tzId!=null && !forecastData.location.tzId!!.isEmpty()){
                mainDataDescTv?.text = forecastData.location!!.tzId!!

            }else{
                mainDataDescTv?.text = "Your location"

            }

            mAdapter= MainAdapter(forecastData.forecast!!.forecastday!!)
            mainDataRv?.adapter = mAdapter

            dataLayout?.turnVisible()

            mainDataRv?.startAnimation(AnimationUtils.loadAnimation(this,R.anim.slide_up))



        }


    }
    override fun hideData() {
        runOnUiThread {
            dataLayout?.turnGone()
        }

    }

    override fun showError(message: String) {
        runOnUiThread {
            errorLayout?.turnVisible()
            errorMessageTv?.text =message
        }

    }

    override fun removeError() {
        runOnUiThread {
            errorMessageTv?.text =""
            errorLayout?.turnGone()
        }

    }

    override fun getData() {
        viewModel?.getWeatherForLocation(4)
    }



}
