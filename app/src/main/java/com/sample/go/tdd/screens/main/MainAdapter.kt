package com.sample.go.tdd.screens.main

import android.content.Context
import android.graphics.drawable.Animatable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.FrameLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.sample.go.tdd.R
import com.sample.go.tdd.data.models.ForecastData
import com.sample.go.tdd.data.models.ForecastdayItem
import java.text.SimpleDateFormat
import java.util.*


class MainAdapter(private val list: List<ForecastdayItem>) : RecyclerView.Adapter<MainAdapter.MyViewHolder>() {

    val format = SimpleDateFormat("yyyy-MM-dd")
    val dayFormat = SimpleDateFormat("EEEE")


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {

        val mInflater = parent.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = mInflater.inflate(R.layout.row_temp, parent, false)




        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {

        holder.nameTv.text = parseDateIntoDay(list[position].date!!)
        holder.dataTv.text = "${list[position].day!!.avgtempC!!}\u2103"


    }

    override fun getItemCount(): Int {
        return list.size
    }

    inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val nameTv: TextView = view.findViewById(R.id.rowTempNameTv)
        val dataTv: TextView = view.findViewById(R.id.rowTempDataTv)
    }

    fun parseDateIntoDay(date: String): String {


        return try {
            val d = format.parse(date)
            return dayFormat.format(d)


        } catch (e: Exception) {
            date
        }

    }


}