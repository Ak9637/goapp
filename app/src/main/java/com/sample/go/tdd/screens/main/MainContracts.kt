package com.sample.go.tdd.screens.main

import androidx.lifecycle.LiveData
import com.sample.go.tdd.common.BaseContracts
import com.sample.go.tdd.data.models.ForecastData
import com.sample.go.tdd.data.models.ForecastDataContainer

class MainContracts{


    companion object {
        const val TAG="MainContracts"

    }

    interface ViewModel : BaseContracts.ViewModel {
        fun getWeatherForLocation(days:Int)
        fun listenForLoadingStateChanges():LiveData<Boolean>
        fun listenForErrors():LiveData<String>
        fun listenForForecasts():LiveData<ForecastData>
        fun runSetup()




    }
    interface View: BaseContracts.View{
        fun initialse()
        fun showLoading()
        fun stopLoading()
        fun showData(forecastData: ForecastData)
        fun hideData()
        fun showError(message:String)
        fun removeError()
        fun getData()



    }

}