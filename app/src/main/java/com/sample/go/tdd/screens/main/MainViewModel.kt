package com.sample.go.tdd.screens.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.sample.go.tdd.common.BaseViewModel
import com.sample.go.tdd.data.Repository
import com.sample.go.tdd.data.models.ForecastData
import com.sample.go.tdd.data.models.ForecastDataContainer
import java.util.*

open class MainViewModel (private val repo:Repository) : BaseViewModel(), MainContracts.ViewModel {


    private val isLoadingListener =MutableLiveData<Boolean>()
    private val errorListener =MutableLiveData<String>()
    private val dataListener =MutableLiveData<ForecastData>()




    override fun listenForForecasts(): LiveData<ForecastData> {
        return dataListener

    }


    override fun listenForLoadingStateChanges(): LiveData<Boolean> {
        return isLoadingListener

    }

    override fun listenForErrors(): LiveData<String> {
        return errorListener
    }


    override fun runSetup() {

        repo.getForecastListener().observeForever {
            isLoadingListener.value = false
            when {
                it.forecastData!=null -> dataListener.value = it.forecastData
                it.error!=null -> errorListener.value=it.error
                else -> errorListener.value = "Something went wrong, please try again"
            }
        }


    }


    override fun getWeatherForLocation(days:Int) {
        // Current the value of location is hardcoded to banglore , can be taken as parameter also
        errorListener.value=null
        isLoadingListener.value=true

        repo.getForecastForLocation(12.9716,77.5946,days)
    }

    override fun onCleared() {
        super.onCleared()
    }



}