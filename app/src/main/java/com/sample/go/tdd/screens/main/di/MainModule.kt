package com.sample.go.tdd.screens.main.di

import androidx.lifecycle.ViewModelProvider
import com.sample.go.tdd.common.MyViewModelFactory
import com.sample.go.tdd.data.Repository
import com.sample.go.tdd.screens.main.MainActivity
import com.sample.go.tdd.screens.main.MainContracts
import com.sample.go.tdd.screens.main.MainViewModel
import dagger.Module
import dagger.Provides
import javax.inject.Named

@Module
class MainModule {

    @Provides
    @Named(MainContracts.TAG)
    fun provideMainViewModel(activity: MainActivity, @Named(MainContracts.TAG) viewModelProvider: ViewModelProvider.Factory): MainContracts.ViewModel {
        return ViewModelProvider(activity, viewModelProvider).get(MainViewModel::class.java)
    }

    @Provides
    @Named(MainContracts.TAG)
    fun provideMainViewModelFactory(repo:Repository): ViewModelProvider.Factory {
        return MyViewModelFactory(MainViewModel(repo))
    }
}