package com.sample.go.tdd.screens.main.di

import com.sample.go.tdd.di.PerActivityScope
import com.sample.go.tdd.screens.main.MainActivity
import dagger.Subcomponent
import dagger.android.AndroidInjector

@PerActivityScope
@Subcomponent(modules = [MainModule::class])
interface MainSubComponent : AndroidInjector<MainActivity> {
    @Subcomponent.Builder
    abstract class Builder : AndroidInjector.Builder<MainActivity>()
}