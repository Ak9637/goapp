package com.sample.go.tdd.testing_helpers

import com.google.gson.Gson
import com.sample.go.tdd.data.models.*
import okhttp3.ResponseBody
import android.R.id.message
import okhttp3.MediaType
import okhttp3.Protocol
import retrofit2.Response
import retrofit2.Retrofit


object MockData{

    var gson = Gson()

    val forecastItem1=ForecastdayItem(null,null,null,Day(null,null,100.0,38.2,null,null,null,null,null,null,null,null,null,null))
    val forecastItem2=ForecastdayItem(null,null,null,Day(null,null,102.0,31.2,null,null,null,null,null,null,null,null,null,null))
    val forecastItem3=ForecastdayItem(null,null,null,Day(null,null,104.0,32.2,null,null,null,null,null,null,null,null,null,null))
    val forecastItem4=ForecastdayItem(null,null,null,Day(null,null,106.0,33.2,null,null,null,null,null,null,null,null,null,null))
    val forecastItem5=ForecastdayItem(null,null,null,Day(null,null,108.0,34.2,null,null,null,null,null,null,null,null,null,null))
    val forecastItem6=ForecastdayItem(null,null,null,Day(null,null,110.0,35.2,null,null,null,null,null,null,null,null,null,null))

    val listOfForcastData= listOf(
        forecastItem1,
        forecastItem2,
        forecastItem3,
        forecastItem4,
        forecastItem5,
        forecastItem6)


    val mockForecastData = ForecastData(

        Current(null,null,null,null,null,null,null,null,null,null,38.2,null,null,100.0,null,null,null,null,null,null,null,null,null),
        Alert(null),
        Location(null,null,null,"Bangaluru",null,null,null,null),
        Forecast(listOfForcastData)

    )

    val mockForcastDataContainer=ForecastDataContainer(mockForecastData,null)

    val mockForecastDataJson= gson.toJson(mockForecastData)

    val mockForecastApiResponse=Response.success(mockForecastData)
    val mockFailureForecastApiResponse:Response<ForecastData?> =Response.success(null)



}