package com.sample.go.tdd.utils


import android.content.Context
import android.content.pm.PackageManager

object PermissionUtils{

    fun checkForLocationPermissions(context: Context):Boolean{



        val locationPermission = android.Manifest.permission.ACCESS_FINE_LOCATION
        val isLocationPermissionsGranted = context.checkCallingOrSelfPermission(locationPermission)

        return ( isLocationPermissionsGranted == PackageManager.PERMISSION_GRANTED )

    }
}