package com.sample.go.tdd

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.test.core.app.ApplicationProvider
import com.nhaarman.mockito_kotlin.doReturn
import com.sample.go.tdd.data.Repository
import com.sample.go.tdd.data.models.ForecastData
import com.sample.go.tdd.data.models.ForecastDataContainer
import com.sample.go.tdd.screens.main.MainContracts
import com.sample.go.tdd.screens.main.MainViewModel
import com.sample.go.tdd.testing_helpers.MockData
import junit.framework.Assert.*
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.After
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.*
import org.mockito.Mockito.`when`
import org.mockito.Mockito.verify
import org.mockito.junit.MockitoJUnit
import org.mockito.junit.MockitoRule
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config
import javax.inject.Inject
import javax.inject.Named


@RunWith(RobolectricTestRunner::class)
@Config(manifest=Config.NONE)
open class MainViewModelTest {


    private lateinit var viewModel: MainViewModel

    @Spy
    private lateinit var forecastContainerLiveData:MutableLiveData<ForecastDataContainer>

    private lateinit var isLoadingLiveData :LiveData<Boolean>

    private lateinit var errorLiveData : LiveData<String>

    @Mock
    private lateinit var repo: Repository

    @Rule
    @JvmField
    val mockitoRule: MockitoRule = MockitoJUnit.rule()

    @Before
    fun setUp() {
        `when`(repo.getForecastListener()).thenReturn(forecastContainerLiveData)


        viewModel = MainViewModel(repo)
        viewModel.runSetup()

        isLoadingLiveData = viewModel.listenForLoadingStateChanges()
        errorLiveData = viewModel.listenForErrors()
    }

    @Test
    fun testHappyFlowForForecastData() = runBlocking {
        var isLoading = isLoadingLiveData.value
        isLoading?.let { assertFalse(it) }

        viewModel.listenForForecasts().observeForever {
            assert(it!=null)
        }
        viewModel.getWeatherForLocation(7)


        isLoading = isLoadingLiveData.value
        isLoading?.let { assert(it) }

        forecastContainerLiveData.value = MockData.mockForcastDataContainer

        isLoading = isLoadingLiveData.value
        isLoading?.let { assertFalse(it) }

        val error=errorLiveData.value
        assert(error==null)


        return@runBlocking
    }
}