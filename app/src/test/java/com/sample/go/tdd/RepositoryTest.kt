package com.sample.go.tdd

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.test.core.app.ApplicationProvider
import com.sample.go.tdd.data.Repository
import com.sample.go.tdd.data.RepositoryImplementation
import com.sample.go.tdd.data.models.ForecastData
import com.sample.go.tdd.data.models.ForecastDataContainer
import com.sample.go.tdd.data.remote.ApiService
import com.sample.go.tdd.root.Constants
import com.sample.go.tdd.screens.main.MainActivity
import com.sample.go.tdd.screens.main.MainViewModel
import com.sample.go.tdd.testing_helpers.MockData
import io.reactivex.Observable
import io.reactivex.disposables.Disposable
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.*
import org.mockito.Mockito.*
import javax.inject.Inject
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config
import javax.inject.Named
import org.mockito.junit.MockitoJUnit
import org.mockito.junit.MockitoRule
import org.robolectric.Robolectric
import org.robolectric.android.controller.ActivityController
import retrofit2.Call
import retrofit2.Response
import rx.subjects.PublishSubject
import java.util.concurrent.Callable


@RunWith(RobolectricTestRunner::class)
@Config(manifest = Config.NONE)

open class RepositoryTest {


    @Mock
    private lateinit var apiService: ApiService

    @Rule
    @JvmField
    val mockitoRule: MockitoRule = MockitoJUnit.rule()


    private val key = "2389562365782356"
    private val location = "50.0,70.0"

    private lateinit var repo: RepositoryImplementation



    @Before
    fun setUp() {
        repo = RepositoryImplementation(apiService, key)
    }


    @Test
    fun apiSuccessTest() = runBlocking {
        /*Check if the api is returning proper data when success is expected
        * For this the api mock interceptor is defined in MockInterceptor class
        *
        * Step 1 - Check there is no error
        * Step 2 - Check whether required data is present and in required quantities
        */

        `when`(
            apiService.requestForecastForLocation(
                key,
                location,
                7
            )
        ).thenReturn(Observable.just(MockData.mockForecastApiResponse))


        repo.getForecastListener().observeForever {
            assert(
                it?.forecastData != null
                        && it.forecastData!!.forecast != null
                        && it.forecastData!!.forecast!!.forecastday != null
                        && it.forecastData!!.forecast!!.forecastday!!.size == 6
            )

        }



        apiService.requestForecastForLocation(key, location, 7)
            .subscribeWith(repo.getForcastObserver())




        return@runBlocking
    }

    @Test
    fun apiFailTest() = runBlocking {
        /*Check if the api is returning proper data when success is expected
        * For this the api mock interceptor is defined in MockInterceptor class
        *
        * Step 1 - Check there is no error
        * Step 2 - Check whether required data is present and in required quantities
        */


        `when`(
            apiService.requestForecastForLocation(
                key,
                location,
                -7
            )
        ).thenReturn(Observable.just(MockData.mockFailureForecastApiResponse))

        repo.getForecastListener().observeForever {
            assert(
                it?.error != null && it.error==Constants.DEFAULT_ERROR_MESSAGE
            )

        }



        apiService.requestForecastForLocation(key, location, -7)
            .subscribeWith(repo.getForcastObserver())



        return@runBlocking

    }


}